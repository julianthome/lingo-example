package custresults

import (
	"fmt"
	"strconv"

	"gitlab.com/julianthome/lingo-example/custtypes"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/eval"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/types"
)

type FloatResult struct{ Val float32 }

func (r FloatResult) DeepCopy() eval.Result { return NewFloatResult(r.Val) }
func (r FloatResult) String() string {
	return strconv.FormatFloat(float64(r.Val), 'f', -1, 32)
}
func (r FloatResult) Type() types.Type   { return custtypes.TypeFloat }
func (r FloatResult) Tidy()              {}
func (r FloatResult) Value() interface{} { return r.Val }
func (r *FloatResult) SetValue(value interface{}) error {
	boolVal, ok := value.(float32)
	if !ok {
		return fmt.Errorf("invalid type for Bool")
	}
	r.Val = boolVal
	return nil
}
func NewFloatResult(value float32) *FloatResult {
	return &FloatResult{
		value,
	}
}
