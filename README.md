# Lingo Example: Random Text Generator (RTG)

Example implementing a data-generation engine with
https://gitlab.com/gitlab-org/vulnerability-research/foss/lingo.

You can build this project by running `go build -o rtg` and then run `./lingo-example
script.rtg` to randomly generate data as specified by the RTG script `script.rtg`.

``` bash
➜ ./lingo-example script.rtg
name,balance
Karl,5010
Max,9409.302

Triss,5084.016
Ciri,6616.023
Geralt,2644.2412

Paul,7927.397
Yennefer,8376.258

Carl,444.4548
Ines,4761.621

Carl,3878.7034


```

# License

The MIT License (MIT)

Copyright (c) 2022, Vulnerability Research, GitLab Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
