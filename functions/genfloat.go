package functions

import (
	"fmt"
	"time"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/eval"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/parser"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/types"

	"gitlab.com/julianthome/lingo-example/custresults"

	"math/rand"
)

type FunctionGenfloat struct {
}

func (f *FunctionGenfloat) Desc() (string, string) {
	return fmt.Sprintf("%s%s %s%s",
			string(parser.TokLeftPar),
			f.Symbol(),
			"min max",
			string(parser.TokRightPar)),
		"generate float in rang [min max]"
}

func (f *FunctionGenfloat) Symbol() parser.TokLabel {
	return parser.TokLabel("genfloat")
}

func (f *FunctionGenfloat) Validate(env *eval.Environment, stack *eval.StackFrame) error {
	if stack.Size() != 2 {
		return eval.WrongNumberOfArgs(f.Symbol(), stack.Size(), 2)
	}

	for idx, item := range stack.Items() {
		if item.Type() != types.TypeInt {
			return eval.WrongTypeOfArg(f.Symbol(), idx+1, item)
		}
	}
	return nil
}

func (f *FunctionGenfloat) Evaluate(env *eval.Environment, stack *eval.StackFrame) (eval.Result, error) {
	var result float32
	for !stack.Empty() {
		max := stack.Pop().(*eval.IntResult)
		min := stack.Pop().(*eval.IntResult)

		minval := float32(min.Val)
		maxval := float32(max.Val)

		result = minval + (rand.Float32() * (maxval - minval))
	}

	return custresults.NewFloatResult(result), nil
}

func NewFunctionGenfloat() (eval.Function, error) {
	rand.Seed(time.Now().UnixNano())
	fun := &FunctionGenfloat{}
	parser.HookToken(fun.Symbol())
	return fun, nil
}
