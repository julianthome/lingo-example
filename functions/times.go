package functions

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/eval"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/parser"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/traversal"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/types"
)

type FunctionTimes struct{}

func (f *FunctionTimes) Desc() (string, string) {
	return fmt.Sprintf("%s%s %s%s",
			string(parser.TokLeftPar),
			f.Symbol(),
			"n 'x",
			string(parser.TokRightPar)),
		"loop quoted expression x n times"
}

func (f *FunctionTimes) Symbol() parser.TokLabel {
	return parser.TokLabel("times")
}

func (f *FunctionTimes) Validate(env *eval.Environment, stack *eval.StackFrame) error {
	if stack.Size() != 2 {
		return eval.TooFewArgs(f.Symbol(), 0, 1)
	}

	if stack.GetArgument(0).Type() != types.TypeInt {
		return eval.WrongTypeOfArg(f.Symbol(), 0, stack.GetArgument(0))
	}

	if stack.GetArgument(1).Type() != types.TypeSexpression && stack.GetArgument(1).Type() != types.TypeString {
		return eval.WrongTypeOfArg(f.Symbol(), 1, stack.GetArgument(1))
	}

	return nil
}

func (f *FunctionTimes) Evaluate(env *eval.Environment, stack *eval.StackFrame) (eval.Result, error) {
	iterations := stack.GetArgument(0).(*eval.IntResult)

	var sb strings.Builder

	if stack.GetArgument(1).Type() == types.TypeString {
		expression := stack.GetArgument(1).(*eval.StringResult)
		for i := 0; i < iterations.Val; i++ {
			sb.WriteString(expression.String())
		}
		return eval.NewStringResult(sb.String()), nil
	}

	if stack.GetArgument(1).Type() != types.TypeSexpression {
		return nil, fmt.Errorf("expected Sexpression")
	}

	expression := stack.GetArgument(1).(*eval.SexpressionResult)
	expressionToEvaluate := expression.Exp.SubExpressions[0]

	for i := 0; i < iterations.Val; i++ {
		evaluator := eval.NewEvaluator(env)
		visitor := traversal.NewExpressionWalker(&evaluator)
		err := visitor.Walk(expressionToEvaluate)
		if err != nil {
			return nil, err
		}
		result := evaluator.Result()
		if result.Type() != types.TypeString {
			return nil, fmt.Errorf("string type expected")
		}

		sb.WriteString(result.String())
	}

	stack.Reset()

	return eval.NewStringResult(sb.String()), nil
}

func NewFunctionTimes() (eval.Function, error) {
	fun := &FunctionTimes{}
	parser.HookToken(fun.Symbol())
	return fun, nil
}
