package functions

import (
	"fmt"
	"strconv"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/eval"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/parser"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/types"

	"gitlab.com/julianthome/lingo-example/custresults"
	"gitlab.com/julianthome/lingo-example/custtypes"
)

type FunctionFloat struct{}

func (f *FunctionFloat) Desc() (string, string) {
	return fmt.Sprintf("%s%s %s%s",
			string(parser.TokLeftPar),
			f.Symbol(),
			"x",
			string(parser.TokRightPar)),
		"Converts a float to a result [Internal]"
}

func (f *FunctionFloat) Symbol() parser.TokLabel {
	return custtypes.TokFloat.Label
}

func (f *FunctionFloat) Validate(env *eval.Environment, stack *eval.StackFrame) error {
	if stack.Size() != 1 {
		return eval.TooFewArgs(f.Symbol(), 0, 1)
	}
	if stack.Peek().Type() != types.TypeCharSequence {
		return eval.WrongTypeOfArg(f.Symbol(), 1, stack.Peek())
	}

	floatvals := stack.Peek().(*eval.CharSequenceResult)
	_, err := strconv.ParseFloat(floatvals.String(), 32)
	if err != nil {
		return err
	}

	return nil
}

func (f *FunctionFloat) Evaluate(env *eval.Environment, stack *eval.StackFrame) (eval.Result, error) {
	floatvals := stack.Pop().(*eval.CharSequenceResult)
	floatval, err := strconv.ParseFloat(floatvals.String(), 32)
	return custresults.NewFloatResult(float32(floatval)), err
}

func NewFunctionFloat() (eval.Function, error) {
	fun := &FunctionFloat{}
	return fun, nil
}
