package functions

import (
	"fmt"

	"math/rand"
	"time"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/eval"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/parser"
	"gitlab.com/gitlab-org/vulnerability-research/foss/lingo/types"
)

type FunctionOneof struct{}

func (f *FunctionOneof) Desc() (string, string) {
	return fmt.Sprintf("%s%s %s%s",
			string(parser.TokLeftPar),
			f.Symbol(),
			"s0 ... sN",
			string(parser.TokRightPar)),
		"returns one of argument strings randomly"
}

func (f *FunctionOneof) Symbol() parser.TokLabel {
	return parser.TokLabel("oneof")
}

func (f *FunctionOneof) Validate(env *eval.Environment, stack *eval.StackFrame) error {
	if stack.Empty() {
		return eval.TooFewArgs(f.Symbol(), stack.Size(), 2)
	}

	for idx, item := range stack.Items() {
		if item.Type() != types.TypeString {
			return eval.WrongTypeOfArg(f.Symbol(), idx+1, item)
		}
	}
	return nil
}

func (f *FunctionOneof) Evaluate(env *eval.Environment, stack *eval.StackFrame) (eval.Result, error) {
	argno := rand.Intn(stack.Size())
	result := stack.GetArgument(argno).(*eval.StringResult)
	stack.Reset()
	return result, nil
}

func NewFunctionOneof() (eval.Function, error) {
	rand.Seed(time.Now().UnixNano())
	fun := &FunctionOneof{}
	parser.HookToken(fun.Symbol())
	return fun, nil
}
